%% 'boxfreezing.m': Heat equation solver for a Cartesian box in 2D with freezing/melting.
%
function [T,fl] = boxfreezing
%
%% Load input file
    inputfile;
    %
    %% Variables initialization
    L = xmax - xmin;
    if (usedx)
        n = [L(1)/dx(1)+1,L(2)/dx(2)+1];
    else
        dx = [L(1)/(n(1)-1),L(2)/(n(2)-1)];
    end
    ntot = [n(1)+2*nghosts,n(2)+2*nghosts];
    %
    %% Build grid
    x = meshgrid( ...
        [xmin(1)-nghosts*dx(1):dx(1):xmax(1)+nghosts*dx(1)], ...
        [xmin(2)-nghosts*dx(2):dx(2):xmax(2)+nghosts*dx(2)] ...
                );
    %
    %% Loop indices
    ireal = 1+nghosts:ntot(1)-nghosts;
    jreal = 1+nghosts:ntot(2)-nghosts;
    itot = 1:ntot(1);
    jtot = 1:ntot(2);
    %
    %% Initialize arrays
    T = zeros(ntot);
    T(ireal,jreal) = Ti*ones(n);
    fl = zeros(ntot);
    k = zeros(ntot);
    rho = zeros(ntot);
    cp = zeros(ntot);
    Tn = zeros(n);
    %
    %% Set boundary conditions
    for i = 1:nghosts
        for j = 1+nghosts:ntot(2)-nghosts
            T(i,j) = left.T;
        end
    end
    for i = 1+nghosts:ntot(1)-nghosts
        for j = ntot(2)-nghosts+1:ntot(2)
            T(i,j) = top.T;
        end
    end
    for i = ntot(1)-nghosts+1:ntot(1)
        for j = 1+nghosts:ntot(2)-nghosts
            T(i,j) = right.T;
        end
    end
    for i = 1+nghosts:ntot(1)-nghosts
        for j = 1:nghosts
            T(i,j) = bottom.T;
        end
    end
    %
    %% Print initial information
    fprintf('Grid spacing (dx,dy) = (%f,%f)\n',dx);
    fprintf('# of grid points (nx,ny) = (%d,%d)\n',n);
    %
    %% Loop initialization
    Tcenter = Ti;
    timestep = 0;
    t = 0;
    nprints = 0;
    nheaderprints = 0;
    %
    %% Time loop
    while ((usefinaltemperature && Tcenter > Tf) || (usefinaltime && t < tf))
        %
        %% Advance time and time step #
        timestep = timestep + 1;
        t = t + dt;
        if (usefinaltime && (t > tf))
            dt = t - tf;
            t = tf;
        end
        %
        %% Properties loop
        for i = itot
            for j = jtot
                fl(i,j) = volumefractionhyperbolic(T(i,j),Tm,dT);
                k(i,j) = fractionaverage(fl(i,j),ks,kl);
                rho(i,j) = fractionaverage(rho(i,j),rhos,rhol);
                cp(i,j) = phasetransitionspecificheat(T(i,j),cps,cpl,Lm,Tm,dT);
            end
        end
        %
        %% Temperature loop
        Tn = heatequation2dcentral(n,ntot,nghosts,dx,T,rho,k,cp);
        %
        %% Norms
        Tdiff = Tn(ireal,jreal) - T(ireal,jreal);
        Linf = max(Tdiff,[],'all');
        L2 = mean(Tdiff,'all');
        %
        %% Assign new temperature
        T(ireal,jreal) = Tn(ireal,jreal);
        %
        %% Maybe print header
        timestep
        Tn'
        T'
        if ((timestep == 1) || (useheaderprintmax && ((t - ti) / (tf - ti) > nheaderprints / nmaxheaderprints)) || (~useheaderprintmax && (mod(timestep, headerprintskip) == 0)))
            disp(' ');
            disp(header);
            nheaderprints = nheaderprints + 1;
        end
        %
        %% Maybe print variables
        if ((timestep == 1) || (t == tf) || (useprintmax && ((t - ti) / (tf - ti) > nprints / nmaxprints)) || (~useprintmax && (mod(timestep, printskip) == 0)))
            printvals = eval(printvars);
            fprintf(printspec,printvals);
            nprints = nprints + 1;
        end
        %
        %% Non-convergence error
        if (usefinaltemperature && (timestep > maxtimesteps))
            error("Convergence not reached within max number of iterations.");
        end
    end
end
%
%% Functions
function Tn = heatequation2dcentral(n,ntot,nghosts,dx,T,rho,k,cp)
%
%% x-Sweep
    kdTdx = zeros(ntot);
    for j = 1+nghosts-1:ntot(2)-nghosts+1
        for i = 1+nghosts-1:ntot(1)-nghosts+1
            kdTdx(i,j) = 1/2*k(i,j)*(T(i+1,j)-T(i-1,j))/dx(1);
        end
    end
    for j = 1+nghosts:ntot(2)-nghosts
        for i = 1+nghosts:ntot(1)-nghosts
            Tn(i,j) = T(i,j) + 1/(rho(i,j)*cp(i,j))*1/2*(kdTdx(i+1,j)-kdTdx(i-1,j))/dx(1);
        end
    end
%
%% y-Sweep
    kdTdx = zeros(ntot);
    for i = 1+nghosts-1:ntot(1)-nghosts+1
        for j = 1+nghosts-1:ntot(2)-nghosts+1
            kdTdx(i,j) = 1/2*k(i,j)*(T(i,j+1)-T(i,j-1))/dx(2);
        end
    end
    for i = 1+nghosts:ntot(1)-nghosts
        for j = 1+nghosts:ntot(2)-nghosts
            Tn(i,j) = T(i,j) + 1/(rho(i,j)*cp(i,j))*1/2*(kdTdx(i,j+1)-kdTdx(i,j-1))/dx(2);
        end
    end
end
%
function fl = volumefractionhyperbolic(T,Tm,dT)
    fl = 1/2*(tanh((T-Tm)/dT) + 1);
end
%
function cp = phasetransitionspecificheat(T,cps,cpl,Lm,Tm,dT)
    if (T < Tm)
        cp = cps + 1/2*sech((T-Tm)/dT)^2*Lm;
    else
        cp = cpl + 1/2*sech((T-Tm)/dT)^2*Lm;
    end
end
%
function avg = harmonicaverage(a,b)
    avg = 2*a*b/(a+b);
end
%
function avg = fractionaverage(f,a1,a2)
    avg = (1-f)*a1 + f*a2;
end
