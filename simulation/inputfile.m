%% 'boxinput.m': Input file for box freezing code.
%
% Time stepping
dt = 0.2;
cfl = 0.25;
maxtimesteps = 1e6;
%
% Solve to final time or center point temperature?
usefinaltime = 1;
usefinaltemperature = 0;
%
% Initial/final
if (usefinaltime)
    ti = 0;
    tf = 600;
elseif (usefinaltemperature)
    Tf = 233;
else
    error('Are you running to a final time or center point temperature?')
end
%
% Use dx or # of points to build grid?
usedx = 0;
usenpoints = 1;
%
% Grid
ndim = 2;
if (usenpoints)
    n = [10,10];
elseif (usedx)
    dx = [0.05,0.05];
else
    error('Are you using # of points or dx to build your grid?');
end
nghosts = 2;
xmin = [-0.1,-0.3];
xmax = [0.1,0.3];
%
% Property interpolation
% Tmin = 250;
% Tmax = 350;
% dT = 1;
%
% Properties
Tm = 273;
dT = 20;

% Solid properties
rhos  = 917;
ks  = 2.22;
cps = 2093;
%
% Liquid properties
cpl = 4186;
rhol  = 997;
kl = 0.598;
Lm = 334000;
% hf = -1.5879e7;
% Tₘ = icemeltingtemperature(pw)
%
% Initial conditions
Ti = 300;
%
% Boundary conditions
top.T = 233;
left.T = 300;
bottom.T = 233;
right.T = 300;
%
% Variable printing header
nmaxheaderprints = 1;
useheaderprintmax = 1;
header = 'timestep t dt Linf L2';
headerprintskip = 10;
%
% Variable printing
useprintmax = 1;
nmaxprints = 30;
printskip = 10;
printvars = '[timestep,t,dt,Linf,L2]';
printspec = '%d %f %f %f %f\n';
